const express = require('express');
const cors = require('cors');
const next = require('next');
const sitemapAndRobots = require('./sitemap-robots');

const dev = process.env.NODE_ENV !== 'production';

const port = process.env.PORT || 3000;
const ROOT_URL = dev
  ? `http://localhost:${port}`
  : 'https://tedxitb.com/';

const app = next({ dev });
const handle = app.getRequestHandler();

// Nextjs's server prepared
app.prepare().then(() => {
  const server = express();

  server.use(cors());

  sitemapAndRobots({ server });

  server.get('*', (req, res) => handle(req, res));

  // starting express server
  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on ${ROOT_URL}`); // eslint-disable-line no-console
  });
});