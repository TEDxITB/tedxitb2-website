const fs = require('fs');
const { join } = require('path');
const { promisify } = require('util');

const copyFile = promisify(fs.copyFile);

const withCss = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');

module.exports = withSass(
  withCss({
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: '[local]',
    },
    async exportPathMap(defaultPathMap, { dev, dir, outDir }) {
      if (dev) {
        return defaultPathMap;
      }

      await copyFile(join(dir, 'robots.txt'), join(outDir, 'robots.txt'));
      await copyFile(join(dir, 'sitemap.xml'), join(outDir, 'sitemap.xml'));

      return defaultPathMap;
    }
  }),
);