User-agent: *
Allow: /
Allow: /speakers
Allow: /about
Allow: /attend
Disallow: /attend_test
Disallow: /thank-you

Sitemap: https://tedxitb.com/sitemap.xml