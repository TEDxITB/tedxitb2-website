require('dotenv').config();

const prod = process.env.NODE_ENV === 'production';

module.exports = {
  'process.env.PRODUCTION': prod,
  'process.env.SITE_RECAPTCHA_KEY': process.env.SITE_RECAPTCHA_KEY,
  'process.env.FORM_NAME': process.env.FORM_NAME,
};