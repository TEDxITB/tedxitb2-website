import * as Yup from 'yup';

import size from 'lodash/size';
import words from 'lodash/words';

Yup.addMethod(Yup.string, 'maxWords', function maxWordsMethod(max, message) {
  return this.test('maxWords', message, function maxWordsTest(value) {
    const { createError } = this;

    return size(words(value)) <= max || createError(message);
  });
});

Yup.addMethod(Yup.string, 'minWords', function minWordsMethod(min, message) {
  return this.test('minWords', message, function minWordsTest(value) {
    const { createError } = this;

    return size(words(value)) >= min || createError(message);
  });
});
