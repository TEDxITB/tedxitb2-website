import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav as NavStrap,
  NavItem,
  NavLink,
} from 'reactstrap';
import Link from 'next/link';

import './index.css';

export default class Nav extends React.Component {
  state = {
    isOpen: false,
  };

  toggle = () => {
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  }

  render() {
    const { isOpen } = this.state;

    return (
      <Navbar color="dark" fixed="top" light expand="lg" className="navbar-ted">
        <NavbarBrand href="/">
          <Link href="/">
            <img className="nav-logo" src="/static/logo-header-2.png" />
          </Link>
        </NavbarBrand>
        <NavbarToggler onClick={this.toggle} className="custom-toggler" />
        <Collapse isOpen={isOpen} navbar>
          <NavStrap navbar className="ml-auto">
            <NavItem className="aboutus-nav">
              <Link href="/about">
                <NavLink href="/about">
                  About
                </NavLink>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/speakers">
                <NavLink href="/speakers">
                  Speakers
                </NavLink>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/venue">
                <NavLink href="/venue">
                  Venue
                </NavLink>
              </Link>
            </NavItem>

            <NavItem>
              <Link href="/gallery">
                <NavLink href="/gallery">
                  Gallery
                </NavLink>
              </Link>
            </NavItem>
            
            <NavItem className="vr-container">
              <div className="vertical-rule" />
            </NavItem>

            <NavItem>
              <Link href="/attend">
                <NavLink href="/attend">
                  Attend
                </NavLink>
              </Link>
            </NavItem>

          </NavStrap>
        </Collapse>
        <style jsx>
          {`
          .navbar-ted {
            zIndex: 999;
            marginBottom: 10;
          }
        `}

        </style>
      </Navbar>
    );
  }

};
