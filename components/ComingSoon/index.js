import { Row, Col } from 'reactstrap';

import './index.scss';

const ComingSoon = () => (
  <Row className="h-100 text-white justify-content-center align-items-center">
    <Col xs="10" sm="5">
      <Row className="grid-divider justify-content-center align-items-center">
        <Col xs="12" sm="7">
          <img src="/static/logo-header-2.png" className="animated img-fluid rounded fadeInLeft" alt="TedxITB Logo" />
        </Col>
        <Col xs="12" sm="5" className="animated justify-content-center fadeInComingSoon delay-1s">
          <h2 className="coming-soon mt-3 my-sm-auto py-0 py-sm-4">
            Coming Soon
          </h2>
        </Col>
      </Row>
    </Col>
  </Row>
);

export default ComingSoon;