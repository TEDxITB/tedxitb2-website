import { Component } from 'react';
import { Row, Col } from 'reactstrap';
import AOS from 'aos';
import 'aos/dist/aos.css';
import './index.scss';

class About extends Component {
  componentDidMount() {
    AOS.init();
    AOS.refresh();
  }

  render() {
    return (
      <div className="sumbiatch" style={{ color: 'white', width: '100%', zIndex: '-1' }}>
        <Row className="about-TEDx today-section" noGutters>
          <Col id="today-section-img" data-aos="fade" data-aos-once="true" data-aos-duration="4000" data-aos-delay="250" className="section-img" sm="6" xs="12" />
          <Col data-aos="fade-left" data-aos-anchor="#today-section-img" data-aos-once="true" data-aos-duration="4000" data-aos-delay="250" className="section-text" sm="6" xs="12">
            <div className="para-container">
              <p>
                <span className="bold red">TED</span>
                {' '}
                is a global community, welcoming people from every discipline and culture who seek a deeper understanding of the world. We believe passionately in the power of ideas to change attitudes, lives and, ultimately, the world. On
                {' '}
                <span className="bold red">TED</span>
                .com, we&apos;re building a clearinghouse of free knowledge from the world&apos;s most inspired thinkers — and a community of curious souls to engage with ideas and each other, both online and at
                {' '}
                <span className="bold red">TED</span>
                {' '}
                and
                {' '}
                <span className="bold red">TEDx</span>
                {' '}
                events around the world, all year long.
              </p>
              <p>
                In fact, everything we do — from our Conferences to our
                {' '}
                <span className="bold red">TED</span>
                {' '}
                Talks to the projects sparked by the
                {' '}
                <span className="bold red">TED</span>
                {' '}
                Prize, from the global
                {' '}
                <span className="bold red">TEDx</span>
                {' '}
                community to the TED-Ed lesson series — is driven by this goal: How can we best spread great ideas?
              </p>
              <p>
                <span className="bold red">TED</span>
                {' '}
                is owned by a nonprofit, nonpartisan foundation. Our agenda is to make great ideas accessible and spark conversation.
              </p>
            </div>
          </Col>
        </Row>

        <Row className="about-TEDx prev-section" noGutters="true">
          <Col id="prev-section-img" data-aos="fade" data-aos-once="true" data-aos-anchor-placement="top-center" data-aos-duration="4000" className="section-img" sm="6" xs="12" />
          <Col data-aos="fade-right" data-aos-anchor="#prev-section-img" data-aos-once="true" data-aos-anchor-placement="top-center" data-aos-duration="4000" className="section-text" sm="6" xs="12">
            <div className="para-container">
              <p>
                This
                {' '}
                <span className="bold red">TEDx</span>
                <span className="bold white">ITB 1.0</span>
                {' '}
                is considered as the seed of the
                {' '}
                <span className="bold red">TEDx</span>
                <span className="bold white">ITB</span>
                {' '}
                event. When the seed grows, what you expect is a diversity of topics that can be regarded as holistic and universal. Some examples of the potential topics that correlates to the theme are happiness, empowerment, breaking the social stigma, sustainable development, technology, and many more. This would ensure that the receivers of the ideas would grasp more than just one topic.
              </p>
              <p>A movement so vivid in meaning yet so subtle in its form that it would touch even the darkest of heart. Beyond the grasp seek to invite minds that dare to stand in the gray area of life to speak out the truth and share ideas that are truly worth spreading.</p>
            </div>
          </Col>
        </Row>

        <Row className="about-TEDx next-section" noGutters="true">
          <Col id="next-section-img" data-aos="fade" data-aos-once="true" data-aos-anchor-placement="top-bottom" data-aos-duration="4000" data-aos-delay="500" className="section-img" sm="6" xs="12" />
          <Col data-aos="fade-left" data-aos-anchor="#next-section-img" data-aos-once="true" data-aos-anchor-placement="top-bottom" data-aos-duration="4000" data-aos-delay="500" className="section-text" sm="6" xs="12">
            <div className="para-container">
              <p>
                Following our success on the very first
                {' '}
                <span className="bold red">TEDx</span>
                <span className="bold white">ITB</span>
                {' '}
                on last May that received enormous enthusiasm and captivate many interests, we proudly present that
                {' '}
                <span className="bold red">TEDx</span>
                <span className="bold white">ITB</span>
                {' '}
                is coming back.
                {' '}
                <span className="bold red">TEDx</span>
                <span className="bold white">ITB 2.0</span>
                {' '}
                would like to continue the initiation of the first
                {' '}
                <span className="bold red">TEDx</span>
                <span className="bold white">ITB</span>
                {' '}
                by delivering the same high spirit and commitment to strive to spread ideas Institut Teknologi Bandung.
              </p>
            </div>
          </Col>
        </Row>

        <Row className="about-TEDx sponsor">
          <Col>
            <h1><span className="red">TEDx</span>ITB 2.0 is sponsored by</h1>
            <div id="large-container">
              <div className="img-container"><img src="/static/about/sponsors/kemenpar.png" width={200} /></div>
              <div className="img-container"><img src="/static/about/sponsors/phm.png" width={250} /></div>
              <div className="img-container"><img src="/static/about/sponsors/nivea.png" width={200} /></div>
              <div className="img-container"><img src="/static/about/sponsors/nivea-men.png" width={200} /></div>
              <div className="img-container"><img src="/static/about/sponsors/hansaplast.png" width={200} /></div>
              <div className="img-container"><img src="/static/about/sponsors/paragon.png" width={200} /></div>
            </div>
            <div id="medium-container">
              <div className="img-container"><img src="/static/about/sponsors/bni.png" width={150} /></div>
              <div className="img-container"><img src="/static/about/sponsors/yap.png" width={150} /></div>
            </div>
            <div id="small-container">
              <div className="img-container"><img src="/static/about/sponsors/hb.png" width={100} /></div>
              <div className="img-container"><img src="/static/about/sponsors/denso.png" width={100} /></div>
              <div className="img-container"><img src="/static/about/sponsors/ipc.png" width={100} /></div>
            </div>
          </Col>
        </Row>
        <style>{`
          .sumbiatch * {
            overflow-y: hidden;
          }
        `}</style>
      </div>
    );
  }
};

export default About;
