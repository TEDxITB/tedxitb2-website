import { Component } from 'react';
import { Row, Col } from 'reactstrap';
import AOS from 'aos';
import 'aos/dist/aos.css';

import './index.scss';

class Speakers extends Component {
  componentDidMount() {
    AOS.init();
    AOS.refresh();
  }

  render() {
    return (
      <div className="sumbiatch">
        <Row noGutters>
          <Col className="nyoman-section speaker-section" sm="12" xs="12">
            <Row style={{height: '20%'}}>
              <Col data-aos="fade-down" data-aos-once="true" data-aos-duration="4000" data-aos-delay="250" className="title-img-parent" md="12" xs="12">
                <img className="title-img" src="/static/speakers/title-speakers.png" />
              </Col>
            </Row>
            <Row style={{height: '80%', paddingBottom: '40px'}}>
              <Col data-aos="fade-right" data-aos-once="true" data-aos-duration="4000" data-aos-delay="250" className="nyoman-img-parent" md="6" xs="12">
                <img className="nyoman-img" src="/static/speakers/nyoman-cropped.png" height="306.31" width="328.25" />
              </Col>
              <Col data-aos="fade-left" data-aos-anchor="nyoman-img-parent" data-aos-once="true" data-aos-duration="4000" data-aos-delay="250" className="nyoman-name" md="6" xs="12">
                <div className="red-text">NYOMAN</div>
                <div className="white-text">NUARTA</div>
                <p>Nyoman Nuarta was born in Tabanan, 14 November 1951. He grew up very close to nature and learned the importance of guarding the harmony between human and The Creator, human with nature and human to human relationship (Tri Hita Kirana). In August 2018, he completed his most ambitious project the “Garuda Wisnu Kencana” monument in Bali which stands 121 m above the ground, marking it as the third tallest monument in the world. Nuarta’s contribution in arts and culture has been appreciated in many ways. Amongst many other, he was awarded “Ganesha Widya Jasa Adiutama” in 2009 and 2018 by Bandung Institute of Technology and “Satyalancana Kebudayaan” in 2014 by the President of Indonesia. </p>
              </Col>
            </Row>
          </Col>
          <Col className="gadis-section speaker-section" sm="12" xs="12">
            <Row style={{height: '100%'}}>
              <Col data-aos="fade-right" data-aos-anchor=".gadis-img-parent" data-aos-anchor-placement="top-center" data-aos-once="true" data-aos-duration="4000" className="gadis-name web-size" md="6" xs="12">
                <div className="fname">
                  <div className="red-text">GADIS</div>
                  <div className="white-text">AZZAHRA</div>
                </div>
                <div className="white-text">PRAMESWARI</div>
                <p>With her incredible ethos, at the age of 13, she was honoured by Ashoka, an international NGO, as Ashoka Young Changemakers for a movement that she made to empower the woman to ride bicycle as a symbol of woman’s liberation. Gadis and her social movements have been featured in Nylon Magazine, Hai! Magazine, Gadis Magazine, Pikiran Rakyat, Kompas, Metro TV, DAAI TV, and she becane one of the youngest Adjunct Lecturer at SBM-ITB at the age of 21. </p>
              </Col>
              <Col data-aos="fade-left" data-aos-anchor-placement="top-center" data-aos-once="true" data-aos-duration="4000" className="gadis-img-parent" md="6" xs="12">
                <img className="gadis-img" src="/static/speakers/gadis-cropped.png" height="300" width="400" />
              </Col>
              <Col data-aos="fade-right" data-aos-anchor=".gadis-img-parent" data-aos-anchor-placement="top-center" data-aos-once="true" data-aos-duration="4000" className="gadis-name mobile-size" md="6" xs="12">
                <div className="fname">
                  <div className="red-text">GADIS</div>
                  <div className="white-text">AZZAHRA</div>
                </div>
                <div className="white-text">PRAMESWARI</div>
                <p>With her incredible ethos, at the age of 13, she was honoured by Ashoka, an international NGO, as Ashoka Young Changemakers for a movement that she made to empower the woman to ride bicycle as a symbol of woman’s liberation. Gadis and her social movements have been featured in Nylon Magazine, Hai! Magazine, Gadis Magazine, Pikiran Rakyat, Kompas, Metro TV, DAAI TV, and she becane one of the youngest Adjunct Lecturer at SBM-ITB at the age of 21. </p>
              </Col>
            </Row>
          </Col>
          <Col className="loangga-section speaker-section" sm="12" xs="12">
            <Row style={{height: '100%'}}>
              <Col data-aos="fade-right" data-aos-anchor-placement="top-center" data-aos-once="true" data-aos-duration="4000" className="loangga-img-parent" md="6" xs="12">
                <img className="loangga-img" md="6" xs="12" src="/static/speakers/loangga-cropped.png" height="300" width="400" />
              </Col>
              <Col data-aos="fade-left" data-aos-anchor=".loangga-img-parent" data-aos-anchor-placement="top-center" data-aos-once="true" data-aos-duration="4000" className="loangga-name" md="6" xs="12">
                <div className="red-text">LOANGGA</div>
                <div className="white-text">ADHISTELLA</div>
                <p>Through “The art of living”, Loangga offers a 180 degree view on how to view and appreciate life. Loangga is one of undergraduate students ITB, but his compelling ideas are truly worth spreading for all of us.</p>
              </Col>
            </Row>
          </Col>
          <Col className="salman-section speaker-section" sm="12" xs="12">
            <Row style={{height: '100%'}}>
              <Col data-aos="fade-right" data-aos-anchor=".salman-img-parent" data-aos-anchor-placement="top-bottom" data-aos-once="true" data-aos-duration="4000" data-aos-delay="500" className="salman-name web-size" md="6" xs="12">
                <div className="red-text">SALMAN</div>
                <div className="white-text">SUBAKAT</div>
                <p style={{textSize: '10pt'}}>Salman Subakat is ITB Electrics and Electronic Engineering alumni in 2002. Now being the spearhead of PT Paragon&quot;s marketing, this bright individual brings a whole new nuance to our conference!</p>
              </Col>
              <Col data-aos="fade-left" data-aos-anchor-placement="top-bottom" data-aos-once="true" data-aos-duration="4000" data-aos-delay="500" className="salman-img-parent" md="6" xs="12">
                <img className="salman-img" md="6" xs="12" src="/static/speakers/salman-cropped.png" height="300" width="400" />
              </Col>
              <Col data-aos="fade-right" data-aos-anchor=".salman-img-parent" data-aos-anchor-placement="top-bottom" data-aos-once="true" data-aos-duration="4000" data-aos-delay="500" className="salman-name mobile-size" md="6" xs="12">
                <div className="red-text">SALMAN</div>
                <div className="white-text">SUBAKAT</div>
                <p>Salman Subakat is ITB Electrics and Electronic Engineering alumni in 2002. Now being the spearhead of PT Paragon&quot;s marketing, this bright individual brings a whole new nuance to our conference!</p>
              </Col>
            </Row>
          </Col>
        </Row>
        <style>
          {`
          @media (max-width: 768px) {
            .sumbiatch * {
              overflow-y: hidden;
            }
          } 
        `}
        </style>
      </div>
    );
  }
}

export default Speakers;