import { Row, Col } from 'reactstrap';

import './index.scss';

import SlideShow from './SlideShow';

const Venue = () => (
  <div style={{ color: 'white', width: '100%', zIndex: '-1' }}>
    <SlideShow />
    <Row className="venue-maps" noGutters>
      <Col className="maps-container" xs="12">
        <iframe
          id="maps-iframe"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.109708206215!2d107.57025031423281!3d-6.87745766920382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e427bac55b75%3A0x28f4666f7db710ba!2sNuArt+Sculpture+Park!5e0!3m2!1sen!2sid!4v1542472847840"
          width="100%"
          height="100%"
          frameBorder="0"
          style={{ border: 0 }}
          allowFullScreen
        />
      </Col>
    </Row>
  </div>
);

export default Venue;
