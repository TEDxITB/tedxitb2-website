import { Component } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';

import range from 'lodash/range';

import {
  Row,
  Col,
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
} from 'reactstrap';

export default class SlideShow extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.images = range(1, 8).map(id => ({
      id,
      source: `/static/venue/${id}`,
      style: `
        .section-img-${id} {
            background-image: linear-gradient(
                to right,
                transparent 0%,
                transparent 50%,
                #1f1e1c
              ),
              url('/static/venue/nuart${id}.jpg');
          }

          @media screen and (max-width: 425px) {
            .section-img-${id} {
              height: 48vh;
              background-image: linear-gradient(
                  to bottom,
                  #1f1e1c,
                  transparent 1%,
                  transparent 0%,
                  transparent 50%,
                  #1f1e1c
                ),
                url('/static/venue/nuart${id}.jpg');
            }
          }
      `,
    }));
  }

  onExiting = () => {
    this.animating = true;
  };

  onExited = () => {
    this.animating = false;
  };

  next = () => {
    const { activeIndex } = this.state;
    if (this.animating) return;
    const nextIndex =
      activeIndex === this.images.length - 1 ? 0 : activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  previous = () => {
    const { activeIndex } = this.state;
    if (this.animating) return;
    const nextIndex =
      activeIndex === 0 ? this.images.length - 1 : activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  };

  goToIndex = newIndex => {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  };

  componentDidMount() {
    AOS.init();
    AOS.refresh();
  }

  render() {
    const { activeIndex } = this.state;

    const slides = this.images.map(item => (
      <CarouselItem
        className={`carousel-img section-img-${item.id}`}
        tag="div"
        key={item.id}
        onExiting={this.onExiting}
        onExited={this.onExited}
      >
        <style scoped type="text/css">
          {item.style}
        </style>
      </CarouselItem>
    ));

    return (
      <div>
        <style type="text/css">
          {`
            .carousel-img {
              background-repeat: no-repeat;
              background-position: center;
              background-size: cover;
              height: 92vh;
            }
          `}
        </style>
        <Row className="venue" noGutters>
          <Col sm="7" xs="12">
            <Carousel
              activeIndex={activeIndex}
              next={this.next}
              previous={this.previous}
            >
              <CarouselIndicators
                items={this.images}
                activeIndex={activeIndex}
                onClickHandler={this.goToIndex}
              />
              {slides}
              <CarouselControl
                direction="prev"
                directionText="Previous"
                onClickHandler={this.previous}
              />
              <CarouselControl
                direction="next"
                directionText="Next"
                onClickHandler={this.next}
              />
            </Carousel>
          </Col>
          <Col data-aos="fade" data-aos-duration="4000" data-aos-delay="250" className="section-text" sm="5" xs="12">
            <div className="para-container">
              <h1 style={{ color: 'red' }}>
                <a
                  href="https://nuartsculpturepark.com"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  NuArt
                </a>
              </h1>
              <h3>Sculpture Park</h3>
              <br />
              <p>
                Located at northern Bandung, West Java, Indonesia, NuArt
                Sculpture Park was first opened for public in the year of 2000.
              </p>
              <p>
                Like its name, NuArt Sculpture Park primarily exhibits the works
                of the sculptor Nyoman Nuarta that spans from the begining of
                his career to the latest masterpieces.
              </p>
              <p>
                The 3 ha park was specifically designed and developed for art
                lovers. It offers limitless resources for those who seek
                knowledge in art and design.
              </p>
              <p>
                {`With NuArt building as the park's landmark, Nuarta has created a
            very special place for artists, designers, and other art-goers to
            meet, discuss, and share their common interests.`}
              </p>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
