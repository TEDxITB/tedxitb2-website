import propTypes from 'prop-types';
import Nav from '@components/Nav';

const Layout = ({ children }) => (
  <div className="layout">
    <Nav />
    <div className="layout-container">
      {children}
    </div>
    <style jsx>
      {`
      .layout {
        display: flex;
        height: 100%;
      }
      .layout-container {
        flex: 1;
        margin-top: 64px;
      }
    `}
    </style>
  </div>
);

Layout.propTypes = {
  children: propTypes.oneOfType([
    propTypes.arrayOf(propTypes.node),
    propTypes.node
  ]).isRequired
};

export default Layout;