import * as Yup from 'yup';

export default {
  summary: {
    section: 2,
    totalFields: 14,
  },
  sections: [
    {
      section: 'Personal Identity',
      instruction: `
        <p class="text-center-desktop">Are you ready for <span class="bold red">TEDx</span><span class="bold white">ITB 2.0</span>!?</p>
        First, we would like to know more about you! In the section below please provide honest data,
        and please make sure you write down the correct email. We would not want to miss informing you
        about any information through email later on!
      `,
      fields: [
        {
          name: 'name',
          label: 'Name',
          type: 'text',
        },
        {
          name: 'email',
          label: 'Email',
          type: 'email',
        },
        {
          name: 'dob',
          label: 'Date of Birth',
          type: 'date',
        },
        {
          name: 'city',
          label: 'Where do you live?',
          type: 'text',
        },
        {
          name: 'occupation',
          label: 'Occupation',
          type: 'text',
        },
        {
          name: 'institution',
          label: 'Institution (Highschool Name, College Name, or Workplace)',
          type: 'text',
        },
        {
          name: 'phone',
          label: 'Phone No.',
          type: 'text',
        },
        {
          name: 'line',
          label: 'LINE',
          type: 'text',
        },
        {
          name: 'instagram',
          label: 'Instagram <span class="bold white">(Optional)</span>',
          type: 'text',
        },
        {
          name: 'linkedin',
          label: 'LinkedIn <span class="bold white">(Optional)</span>',
          type: 'text',
        },
        {
          name: 'nepotism',
          label:
            'Are you previously/currently affiliated with TEDxITB? If so, please state whether you are: TEDxITB 1.0 committee, TEDxITB 1.0 volunteer, or TEDxITB 2.0 Influencer <span class="bold white">(Optional)</span>',
          type: 'text',
        },
      ],
    },
    {
      section: 'Motivation & Competencies',
      instruction: `
      This next session, we want to get to know you even deeper. <br />
      What your perspectives are, how open-minded you are, and how motivated you are to receive and share new ideas! <br />
      Please be as elaborative as you can, while also being effective in the writing.
      `,
      fields: [
        {
          name: 'why-tedxitb',
          label:
            'Why <span class="bold red">TEDx</span><span class="bold white">ITB</span>? <span class="bold white">(max. 100 words)</span>',
          type: 'textarea',
        },
        {
          name: 'what-makes-you',
          label:
            'What makes you, you? <span class="bold white">(max. 100 words)</span>',
          type: 'textarea',
        },
        {
          name: 'open-mindedness',
          label:
            'If you achieve great things, would you help others do the same, and why? <span class="bold white">(max. 200 words)</span>',
          type: 'textarea',
        },
        {
          name: 'creativity',
          label:
            'What is beauty to you? <span class="bold white">(max. 200 words)</span>',
          type: 'textarea',
        },
      ],
    },
  ],
  validation: Yup.object().shape({
    name: Yup.string()
      .min(3, 'Please enter more than 3 characters')
      .required('Name is required.'),
    email: Yup.string()
      .email('Invalid email address')
      .required('Email is required.'),
    dob: Yup.date().required('Date of Birth is required.'),
    city: Yup.string()
      .min(3, 'Please enter more than 3 characters')
      .required('City is required.'),
    occupation: Yup.string()
      .min(3, 'Please enter more than 3 characters')
      .required('Occupation is required.'),
    institution: Yup.string()
      .min(3, 'Please enter more than 3 characters')
      .required('Institution is required.'),
    phone: Yup.string()
      .matches(/^\d+$/, 'Please enter number only.')
      .min(8, 'Please enter more than 8 numbers')
      .required('Institution is required.'),
    line: Yup.string().required('Line username is required.'),
    instagram: Yup.string(),
    linkedin: Yup.string(),
    nepotism: Yup.string(),
    'why-tedxitb': Yup.string()
      .maxWords(100, 'Please enter no more than 100 words')
      .required('This question is required.'),
    'what-makes-you': Yup.string()
      .maxWords(100, 'Please enter no more than 100 words')
      .required('This question is required.'),
    'open-mindedness': Yup.string()
      .maxWords(200, 'Please enter no more than 200 words')
      .required('This question is required.'),
    creativity: Yup.string()
      .maxWords(200, 'Please enter no more than 200 words')
      .required('This question is required.'),
    'cv-consent': Yup.boolean()
      .oneOf([true], 'You must check this field!')
      .required('You must check this field!'),
    'priv-consent': Yup.boolean()
      .oneOf([true], 'You must check this field!')
      .required('You must check this field!'),
    'g-recaptcha-response': Yup.string()
      .min(40, 'Please do not skip ReCaptcha validation')
      .required(),
  }),
};
