import PropTypes from 'prop-types';

import { Field, ErrorMessage } from 'formik';

import {
  FormGroup,
  Label,
  Input,
  FormFeedback,
} from 'reactstrap';

const FormField = ({ label, row, type, name, id, ...props }) => (
  <div>
    <Field name={name}>
      {
        ({ field, form: { errors } }) => (
          <FormGroup row={row}>
            <Label className="label-modified" htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />
            <Input
              invalid={
                !!errors[field.name]
              }
              type={type}
              id={name}
              {...props}
              {...field}
            />
            <ErrorMessage component={FormFeedback} name={field.name} />
          </FormGroup>
        )
      }
    </Field>
    <style jsx global>
      {`
        .label-modified {
          color: #ffffff;
        }

        #cv-consent, #priv-consent {
          margin: 0;
          position: static;
        }
      `}
    </style>
  </div>
);

FormField.propTypes = {
  label: PropTypes.string.isRequired,
  row: PropTypes.bool,
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

FormField.defaultProps = {
  row: false,
};

export default FormField;