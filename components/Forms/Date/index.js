import { Fragment } from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';
import range from 'lodash/range';
import { Field, ErrorMessage } from 'formik';
import {
  SingleDatePicker
} from 'react-dates';

import {
  FormGroup,
  Label,
  FormFeedback,
  Row,
  Col,
} from 'reactstrap';

const FormField = ({ label, row, name, setFieldValue, setFieldTouched, ...props }) => (
  <div>
    <Field name={name}>
      {
        ({ field, form: { touched } }) => (
          <FormGroup row={row}>
            <Label className="label-modified" htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />
            <SingleDatePicker
              {...props}
              id="date_input"
              daySize={37}
              numberOfMonths={1}
              date={field.value}
              focused={!!touched[name]}
              onDateChange={date => setFieldValue(field.name, date)}
              onFocusChange={() => setFieldTouched(field.name, !touched[name])}
              displayFormat="DD MMMM YYYY"
              isOutsideRange={() => false}
              placeholder=""
              navPrev={<Fragment />}
              navNext={<Fragment />}
              block
              small
              renderMonthElement={({ month, onMonthSelect, onYearSelect }) => (
                <Row form noGutters className="d-flex justify-content-center">
                  <Col sm={6} xs={6}>
                    <select
                      value={month.month()}
                      onChange={(e) => {
                        onMonthSelect(month, e.target.value);
                      }}
                    >
                      {moment.months().map((_label, value) => (
                        <option key={value} value={value}>{_label}</option>
                      ))}
                    </select>
                  </Col>
                  <Col sm={6} xs={6}>
                    <select
                      value={month.year()}
                      onChange={(e) => {
                        onYearSelect(month, e.target.value);
                      }}
                    >
                      {range(2018, 1900).map((_label) => (
                        <option key={_label} value={_label}>{_label}</option>
                      ))}
                    </select>
                  </Col>
                </Row>
              )}
            />
            <ErrorMessage component={FormFeedback} name={field.name} />
          </FormGroup>
        )
      }
    </Field>
    <style jsx global>
      {`
          .label-modified {
            color: #ffffff;
          }
        `}
    </style>
  </div>
);

FormField.propTypes = {
  label: PropTypes.string.isRequired,
  row: PropTypes.bool,
  name: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
};

FormField.defaultProps = {
  row: false,
};

export default FormField;