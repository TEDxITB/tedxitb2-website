import PropTypes from 'prop-types';

import {
  Alert,
  NavItem,
  NavLink,
  Row,
  Table,
  TabPane,
} from 'reactstrap';

export const InstructionsTab = ({ index, active, toggle }) => (
  <>
    <NavItem>
      <NavLink
        onClick={() => {
          toggle(index);
        }}
        className={active === index ? 'active' : ''}
      >
        Instructions
      </NavLink>
    </NavItem>
  </>
);

InstructionsTab.propTypes = {
  index: PropTypes.number.isRequired,
  active: PropTypes.number.isRequired,
  toggle: PropTypes.func.isRequired,
};

export const InstructionsContent = ({ index }) => (
  <TabPane tabId={index}>
    <Row>
      <Alert className="container-fluid mx-auto px-4 custom-alert text-center-desktop">
        <p>
          <span className="bold red">TEDx</span>
          <span className="bold white">ITB 2.0</span>
          {' '}
          is proud to announce that the current batch audience selection registration is now open!
        </p>
        Please pay attention to the following instructions before you fill in your application.
        <Row />
      </Alert>
    </Row>
    <Row>
      <Table className="mx-sm-1 mx-0" borderless responsive>
        <tbody>
          <tr>
            <th scope="row" style={{ width: '1%' }}>1.</th>
            <td>
              TEDxITB 2.0 will be held on November 24th, 2018, 13:00 - 19:30, at NuArt Sculpture Park, Jl. Setraduta Raya No. L6, Ciwaruga, Bandung.
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>2.</th>
            <td>
              TEDxITB 2.0 registration phase is divided into 3 phases:
              {' '}
              <span className="bold white">First, Second, and Third.</span>
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>3.</th>
            <td>
              First phase is open from
              {' '}
              <span className="bold white">November 2nd – November 9th</span>
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>4.</th>
            <td>
              Second phase is open from
              {' '}
              <span className="bold white">November 9th – November 16th</span>
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>5.</th>
            <td>
              Third phase is open from
              {' '}
              <span className="bold white">November 17th – November 20th</span>
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>6.</th>
            <td>
              Announcement for selected audience will be given via
              {' '}
              <span className="bold white">EMAIL</span>
              {' '}
              registered in the form 1 day after the phase closes,
              therefore please make sure your email is written correctly.
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>7.</th>
            <td>
              After announcement, selected audience have 3 days to complete payment of audience fee. Detail of payment will be included in the acceptance email.
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>8.</th>
            <td>
              Audience fee for each phase is different, starting from
              {' '}
              <span className="bold white">IDR 125k</span>
              {' '}
              for first phase with an increase of
              {' '}
              <span className="bold white">IDR 25k</span>
              {' '}
              for each phase.
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>9.</th>
            <td>
              After 3 days, failure to pay for audience fee will result in cancellation of audience status, and your space will be given to the next phase registration quota.
            </td>
          </tr>
          <tr>
            <th scope="row" style={{ width: '1%' }}>10.</th>
            <td>
              You could either pay via normal transfer, or via YAP! First 100 payment through YAP! will be given  
              {' '}
              <span className="bold white">IDR 50k</span>
              {' '}
              discount!
            </td>
          </tr>
        </tbody>
      </Table>
    </Row>
  </TabPane>
);

InstructionsContent.propTypes = {
  index: PropTypes.number.isRequired,
};