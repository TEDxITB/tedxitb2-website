import { Component } from 'react';
import Router from 'next/router';
import PropTypes from 'prop-types';

import { Formik } from 'formik';
import {
  Alert,
  Card,
  CardHeader,
  CardBody,
  Col,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Form,
  Input as BootstrapInput,
  Nav,
  NavItem,
  NavLink,
  Row,
  Progress,
  Table,
  TabContent,
  TabPane,
} from 'reactstrap';

import Input from '@components/Forms/Input';
import Date from '@components/Forms/Date';
import ReCaptcha from '@components/Forms/ReCaptcha';

import encode from '@utils/encode';

import { InstructionsTab, InstructionsContent } from './instructions';

export const tabGenerator = (sections, active, toggle) => (
  <>
    {sections.map((section, idx) => (
      <NavItem key={section.section}>
        <NavLink
          onClick={() => {
            toggle(idx + 1);
          }}
          className={active === (idx + 1) ? 'active' : ''}
        >
          {section.section}
        </NavLink>
      </NavItem>
    ))}
  </>
);

export const formGenerator = (sections, props) => (
  <>
    {sections.map((section, idx) => (
      <TabPane tabId={idx + 1}>
        <Row>
          <Alert className="container-fluid mx-auto px-4" color="secondary">
            <div dangerouslySetInnerHTML={{ __html: section.instruction }} />
          </Alert>
        </Row>
        {section.fields.map(field => (
          <Row key={field.name}>
            <Col sm={12}>
              {
                field.type === 'date' ?
                  <Date {...field} {...props} />
                  :
                  <Input {...field} {...props} />
              }
            </Col>
          </Row>
        ))}
      </TabPane>
    ))}
  </>
);

const RECAPTCHA_KEY = process.env.SITE_RECAPTCHA_KEY;
const RECAPTCHA_KEY_REQUEST = 'g-recaptcha-response';

class FormGenerator extends Component {
  static propTypes = {
    template: PropTypes.shape({
      summary: PropTypes.shape({
        section: PropTypes.number,
        totalFields: PropTypes.number,
      }).isRequired,
      sections: PropTypes.arrayOf([
        PropTypes.shape({})
      ]).isRequired,
      validation: PropTypes.shape({})
    }).isRequired,
    form: PropTypes.string.isRequired,
    initialValues: PropTypes.shape({}),
  }

  static defaultProps = {
    initialValues: {},
  }

  state = {
    activeTab: 0,
    submitError: false,
  }

  handleSubmitError = value => {
    this.setState({
      submitError: value,
    });
  }

  toggleTab = (tab) => {
    const { activeTab } = this.state;

    if (activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    const { template: { summary, sections, validation }, initialValues, form: formName } = this.props;
    const { activeTab, submitError } = this.state;
    const sectionLength = sections.length;
    let hasSubmitBeenClicked = false;
    return (
      <div>
        <Formik
          initialValues={initialValues}
          validationSchema={validation}
          onSubmit={(values, actions) => {
            const date_input = values.dob.format('DD MMMM YYYY');
            fetch('/', {
              method: 'POST',
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
              body: encode({ 'form-name': formName, ...values, 'date_input': date_input })
            })
              .then(() => {
                this.handleSubmitError(false);
                actions.setSubmitting(false);
                Router.push('/thank-you');
              })
              .catch(() => {
                this.handleSubmitError(true);
              });
          }}
          render={({ handleSubmit, values, submitForm, isValid, isSubmitting, isValidating, ...rest }) => {
            const progress = Object.keys(values).length / summary.totalFields * 100;
            return (
              (
                <Form
                  action="/thank-you/"
                  netlify="true"
                  data-netlify="true"
                  data-netlify-honeypot="bot-field"
                  data-netlify-recaptcha="true"
                  name={formName}
                  onSubmit={handleSubmit}
                >
                  <noscript>
                    <p>This form won’t work with Javascript disabled</p>
                  </noscript>
                  <Card className="mx-md-6" body>
                    <CardHeader tag="div">
                      <Nav fill pills tabs>
                        <InstructionsTab index={0} active={activeTab} toggle={this.toggleTab} />
                        {tabGenerator(sections, activeTab, this.toggleTab)}
                      </Nav>
                      {
                        progress > 0 && (
                          <>
                            <Progress
                              className="my-3"
                              animated
                              striped
                              color="success"
                              value={progress}
                            />
                          </>
                        )}
                      {
                        submitError && (
                          <Alert color="danger">
                            <h4 className="alert-heading">Submission failed, please retry :)</h4>
                            <hr />
                            <p>
                              We apologize for the inconvenience :(
                            </p>
                            <p className="mb-0">
                              If this problem still persists, please kindly reach TEDxITB through Line OA or Instagram
                            </p>
                          </Alert>
                        )}
                    </CardHeader>
                    <CardBody>
                      <TabContent activeTab={activeTab}>
                        <InstructionsContent index={0} />
                        {formGenerator(sections, rest)}
                      </TabContent>
                      <BootstrapInput hidden name="bot-field" />
                      <Row form>
                        <Col md="12">
                          { /* eslint-disable-next-line */
                            activeTab === sectionLength ? (
                              <>
                                Please also upload your latest CV through this
                                {' '}
                                <a className="bold red" href="https://www.dropbox.com/request/Subuq6qxb9Lc1LzSIx6R" target="_blank" rel="noopener noreferrer">link</a>
                                {' '}
                                with your name in the file name.
                                <br />
                                <Table className="my-0 py-0" borderless>
                                  <tbody>
                                    <tr>
                                      <th scope="row" style={{ width: '1%' }}><Input type="checkbox" name="cv-consent" {...rest} /></th>
                                      <td>By checking this box, I confirmed that I have uploaded my CV</td>
                                    </tr>
                                  </tbody>
                                </Table>
                                <Table className="my-0 py-0" borderless>
                                  <tbody>
                                    <tr>
                                      <th scope="row" style={{ width: '1%' }}><Input type="checkbox" name="priv-consent" {...rest} /></th>
                                      <td>By checking this box, I confirm that I give my consent for TEDxITB to use my data for partnership use with TEDxITB sponsors</td>
                                    </tr>
                                  </tbody>
                                </Table>
                                <div className="my-0 mb-3">
                                  <ReCaptcha
                                    siteKey={RECAPTCHA_KEY}
                                    name={RECAPTCHA_KEY_REQUEST}
                                    {...rest}
                                  />
                                </div>
                                {!isValid && hasSubmitBeenClicked && (
                                  <Alert color="danger">
                                    Please ensure that you have filled and checked all the mandatory fields correctly.
                                  </Alert>
                                )}
                                <ButtonToolbar className="justify-content-between">
                                  <Button size="lg" onClick={() => this.toggleTab(activeTab - 1 % sectionLength)}>Back</Button>
                                  <Button
                                    size="lg"
                                    type="submit"
                                    color={isValid ? 'success' : 'warning'}
                                    onClick={() => {
                                      if (!isValid) hasSubmitBeenClicked = true;
                                    }}
                                  >
                                    Submit
                                  </Button>
                                </ButtonToolbar>
                              </>
                            )
                              :
                              activeTab === 0 ?
                                <Button size="lg float-right" color="info" onClick={() => this.toggleTab(activeTab + 1 % sectionLength)}>Next</Button>
                                : (
                                  <ButtonToolbar className="justify-content-between">
                                    <Button size="lg" onClick={() => this.toggleTab(activeTab - 1 % sectionLength)}>Back</Button>
                                    <Button size="lg" color="info" onClick={() => this.toggleTab(activeTab + 1 % sectionLength)}>Next</Button>
                                  </ButtonToolbar>
                                )}
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Form>
              )
            );
          }}
        />
      </div>
    );
  }
}

export default FormGenerator;