import PropTypes from 'prop-types';

import Recaptcha from 'react-google-recaptcha';
import { Field, ErrorMessage } from 'formik';

import {
  FormGroup,
  Label,
  FormFeedback,
} from 'reactstrap';

const FormField = ({ label, row, type, name, siteKey, setFieldValue, setFieldError, ...props }) => (
  <div>
    <Field name={name}>
      {
        ({ field }) => (
          <FormGroup row={row}>
            <Label className="label-modified" htmlFor={name}>{label}</Label>
            <Recaptcha
              {...props}
              theme="dark"
              sitekey={siteKey}
              onChange={(value) => setFieldValue(field.name, value)}
              onErrored={() => setFieldError(field.name, 'ReCAPTCHA is error!')}
            />
            <ErrorMessage component={FormFeedback} name={field.name} />
          </FormGroup>
        )
      }
    </Field>
    <style jsx global>
      {`
        .label-modified {
          color: #ffffff;
        }
      `}
    </style>
  </div>
);

FormField.propTypes = {
  label: PropTypes.string,
  row: PropTypes.bool,
  siteKey: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  setFieldError: PropTypes.func.isRequired,
};

FormField.defaultProps = {
  label: '',
  row: false,
};

export default FormField;