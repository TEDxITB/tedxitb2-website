import Layout from '@components/Layout';
import Venue from '@components/Venue';

import './css/coming-soon.scss';

export default () => (
  <Layout>
    <Venue />
  </Layout>
);
