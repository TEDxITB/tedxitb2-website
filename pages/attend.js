
import Layout from '@components/Layout';

import ComingSoonBatch from '@components/ComingSoon/batch2';
import ComingSoon from '@components/ComingSoon';
import FormGenerator from '@components/Forms/Generator';
import BatchOne from '@components/Forms/Templates/BatchOne';
import BatchTwo from '@components/Forms/Templates/BatchTwo';
import BatchThree from '@components/Forms/Templates/BatchThree';

const FORM_NAME = process.env.FORM_NAME || false;

const Attend = () => (
  <Layout>
    {
      FORM_NAME === 'BATCH_ONE' ? <FormGenerator form="attendee" template={BatchOne}/> 
      : (FORM_NAME === 'BATCH_TWO' ? <FormGenerator form="attendee" template={BatchTwo}/> 
      : (FORM_NAME === 'BATCH_THREE' ? <FormGenerator form="attendee" template={BatchThree} /> 
      : <ComingSoonBatch/>))
    }
  </Layout>
);

export default Attend;