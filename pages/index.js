import Layout from '@components/Layout';

export default () => (
  <Layout>
    <div id="post-event">
      <div className="text-container animated fadeInUp">
        <h1 style={{color: 'red', fontSize: '5rem'}}>Thank You!</h1>
        <h2>
see you on the next
          <span style={{color: 'red'}}>TEDx</span>
ITB
        </h2>
      </div>
    </div>
    <style>
      {`
            html {
                overflow: hidden
            }

            @font-face {
                font-family: 'haarlem-deco';
                src: url('/static/fonts/haarlem-deco/Haarlem-Deco.otf') format('opentype'),
                  url('/static/fonts/haarlem-deco/Haarlem-Deco.ttf') format('truetype');
            }

            h1, h2 {
                font-family: haarlem-deco;
            }

            #post-event {
                height: 100vh;
                width: 100vw;
                background-color: #1F1E1C;
                background-image: linear-gradient(to bottom, transparent 10%, #1F1E1C 80%), url("/static/about/3.jpg");
            
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .text-container {
                text-align: center;
                padding-left: 40px;
                padding-right: 40px;
                animation-delay: 700ms
            }
        `}
    </style>
  </Layout>
);