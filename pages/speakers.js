import Layout from '@components/Layout';
import Speakers from '@components/Speakers';

export default () => (
  <Layout>
    <Speakers />
  </Layout>
);