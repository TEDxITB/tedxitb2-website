import React from 'react';
import ErrComp from 'next/error';
import Layout from '@components/Layout';
import ComingSoon from '@components/ComingSoon';

export default class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    const { statusCode } = this.props;

    if (statusCode === 404) {
      return (
        <Layout>
          <ComingSoon />
        </Layout>
      );
    }

    return (
      <ErrComp statusCode={statusCode} />
    );
  }
}