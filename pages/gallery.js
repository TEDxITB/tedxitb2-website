import Layout from '@components/Layout';
import Gallery from '@components/Gallery';

export default () => (
  <Layout>
    <Gallery />
  </Layout>
);
