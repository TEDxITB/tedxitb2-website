import Layout from '@components/Layout';
import About from '@components/About';

import './css/coming-soon.scss';

const ComingSoon = () => (
  <Layout>
    <About />
  </Layout>
);

export default ComingSoon;
