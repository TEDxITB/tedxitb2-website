import Layout from '@components/Layout';
import './css/home.scss';

const CountdownPage = () => (
  <Layout>
    <div id="flex-container">
      <div>
        <h2>Your submission</h2>
        <h2>will be processed.</h2>
        <h2>Thank You!</h2>
        <h2>
          <span className="bold red">TEDx</span>
          <span className="bold white">ITB</span>
        </h2>
      </div>
      <img className="accent" id="boxy" src="/static/countdown/GRAPHIC.png" />
    </div>
  </Layout>
);

export default CountdownPage;