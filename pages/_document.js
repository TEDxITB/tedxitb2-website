// ./pages/_document.js
import Document, { Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <html lang="en">
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/static/favicon.ico" />
          {process.env.PRODUCTION && (
            <link
              href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.3/darkly/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-w+yWASP3zYNxxvwoQBD5fUSc1tctKq4KUiZzxgkBSJACiUp+IbweVKvsEhMI+gz7"
              crossOrigin="anonymous"
            />
          )}
          <link rel="stylesheet" href="/_next/static/style.css" />
          <meta
            name="keywords"
            content="TED, TEDx, ITB, Talks, Themes, Speakers, Technology, Entertainment, Design"
          />
          <meta
            name="description"
            content="A movement so vivid in meaning yet so subtle in its form that it would touch even the darkest of heart. Beyond the grasp seek to invite minds that dare to stand in the gray area of life to speak out the truth and share ideas that are truly worth spreading."
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
