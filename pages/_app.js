import Head from 'next/head';
import App, { Container } from 'next/app';
import Router from 'next/router';

import 'isomorphic-unfetch';
import moment from 'moment';
import 'react-dates/initialize';
import { PageTransition } from 'next-page-transitions';
import NProgress from 'nprogress';

import Loader from '@components/Loader';
import '@utils/yup-override';

/**
 * Style (css/sass)
 */
import 'nprogress/nprogress.css';
import 'react-dates/lib/css/_datepicker.css';
import 'animate.css/animate.min.css';
import './css/_app.scss';

moment().locale('id');

const TIMEOUT = 400;
NProgress.configure({ easing: 'ease', speed: 500 });
Router.events.on('routeChangeStart', () => {
  NProgress.start();
});
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

export default class extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <Container>
        <Head>
          <title>TEDxITB 2.0</title>
        </Head>
        <PageTransition
          timeout={TIMEOUT}
          classNames="page-transition"
          loadingComponent={<Loader />}
          loadingDelay={500}
          loadingTimeout={{
            enter: TIMEOUT,
            exit: 0,
          }}
          loadingClassNames="loading-indicator"
        >
          <Component {...pageProps} />
        </PageTransition>
        <style jsx global>
          {`
            .page-transition-enter {
              opacity: 0;
              display: initial;
              transform: translate3d(0, 20px, 0);
            }

            .page-transition-enter-done {
              flex: 1;
              display: initial;
            }

            .page-transition-enter-active {
              opacity: 1;
              transform: translate3d(0, 0, 0);
              transition: opacity ${TIMEOUT}ms, transform ${TIMEOUT}ms;
            }
            .page-transition-exit {
              opacity: 1;
            }
            .page-transition-exit-active {
              opacity: 0;
              transition: opacity ${TIMEOUT}ms;
            }
            .loading-indicator-appear,
            .loading-indicator-enter {
              opacity: 0;
            }
            .loading-indicator-appear-active,
            .loading-indicator-enter-active {
              opacity: 1;
              transition: opacity ${TIMEOUT}ms;
            }
          `}
        </style>
      </Container>
    );
  }
}
