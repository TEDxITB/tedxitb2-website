import moment from 'moment';

import Layout from '@components/Layout';
import './css/home.scss';

const countdownDays = moment('2018-11-24T15:00:00+0700').diff(new Date(), 'days');

const CountdownPage = () => (
  <Layout>
    <div id="flex-container">
      <div>
        <h1>{countdownDays}</h1>
        <h2>days to go</h2>
      </div>
      <img className="accent" id="boxy" src="/static/countdown/GRAPHIC.png" />
    </div>
    <style>
      {`
      html {
        overflow: hidden;
      }
    `}
    </style>
  </Layout>
);

export default CountdownPage;