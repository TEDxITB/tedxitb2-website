const env = require('./env-config.js');

module.exports = {
  presets: ['next/babel'],
  plugins: [
    [
      "module-resolver",
      {
        "root": [
          "./"
        ],
        "alias": {
          "@components": "./components",
          "@utils": "./utils"
        }
      }
    ],
    ['transform-define', env],
  ]
};